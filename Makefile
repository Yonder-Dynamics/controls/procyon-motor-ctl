
DRIVER = driver/main.py
ARDUINO_CONFIG = arduino_config.json

.PHONY: all arduino run

run: arduino

arduino:
	@ $(DRIVER) $(ARDUINO_CONFIG)

