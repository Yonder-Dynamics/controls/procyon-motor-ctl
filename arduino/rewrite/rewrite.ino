#define BAUDRATE 115200

#define BUF_SIZE 5

enum parse_state { MSG_START, MSG_TYPE, MSG_CONTENTS };

parse_state state;
char buf[BUF_SIZE + 1];
int buf_pos;
int type;

void setup(){
    state = MSG_START;
    buf_pos = 0;
    Serial.begin(BAUDRATE);
}

/*
 * Parsing process:
 * 1. Read characters until a '#' is encountered
 * 2. Read characters into buffer until a ',' is encountered
 * 3. Attempt to parse an integer from the buffer
 * 4. Read csv values into appropriate container until a '!'
 * 
 */

void loop(){
    state = process(state);
}

parse_state process(parse_state state){
    switch(state){
        case MSG_START   : return parse_msg_start();
        case MSG_TYPE    : return parse_msg_type();
        case MSG_CONTENTS: return parse_msg_contents();
    }
}

parse_state parse_msg_start(){
    int c = Serial.read();
    return (c == '#') ? MSG_TYPE : MSG_START;
}

parse_state parse_msg_type(){
    int c = Serial.read();
    if(c == -1){
        return MSG_TYPE;
    }
    if(c == ','){
        // attempt to parse the type
        type = parse_type();
        if(type == 0){
            reset_parser();
            send_error();
            return MSG_START;
        } else {
            return MSG_CONTENTS;
        }
    }
    if(buf_pos == BUF_SIZE){
        // buffer is full but the type section was not terminated
        // abort parsing
        reset_parser();
        send_error();
        return MSG_START;
    }
    buf[buf_pos++] = (char)c;
}

parse_state parse_msg_contents(){
    
}

int parse_type(){
    buf[buf_pos] = 0;
    return atoi(buf_pos);
}

void reset_parser(){
    buf_pos = 0;
}

void send_error(){
    Serial.write("#-1!");
}
