# rover_2018_arduino

Hacky Arduino code written at URC 2018. Archaic and mysterious. We will to move away from this codebase once we have a working alternative. The is a lot of old, unused, and probably broken code left over from old hardware which has since been modified or removed.

This code respects enough of the HashDelimited protocol to be able to communicate with a master computer over a serial connection.

To get started understanding this codebase, find the `loop` function in `rover_2018_arduino.ino`. This is the top level logic running on the Arduino, which can be traced down to the serial parsing used to set wheel speeds, as well as failsafe logic.