# Hash CSV Protocol
Basic ascii-based protocol for communicating with rover microcontrollers.
Designed for speed and robustness over a UART physical layer.

## Description
1. Messages begin with `#`
2. Messages end with `!`
3. Message values are delimited by `,`
4. Valid message contents are in `[0-9.]+`
5. The first message value is a positive integer denoting the message type.
6. Senders may not send additional messages until the receiver sends a response.

## Design Considerations
The protocol was designed with Arduino microcontrollers in mind. The speed of
Arduino processors is particularly important, as is the small size of their
serial buffer (63 bytes). Faster computers can easily overwhelm an Arduino,
creating issues with buffer backlog and overflow. These effects are especially
noticeable in real-time control scenarios, where buffer backlog can lead to
loss of control.