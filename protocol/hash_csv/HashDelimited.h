#ifndef HASH_DELIMITED_PROTOCOL_H
#define HASH_DELIMITED_PROTOCOL_H

#include "Handler.h"

class HashDelimitedProtocol{
    public:
        HashDelimitedProtocol(int numHandlers, Handler** handlers);
        ~HashDelimitedProtocol();

        void listen(int duration);
        void send(int numValues, float* values);
    private:
        Handler** handlers;

};

#endif //HASH_DELIMITED_PROTOCOL_H