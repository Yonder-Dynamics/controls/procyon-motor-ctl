#ifndef HANDLER_H
#define HANDLER_H

class Handler{
    public:
        virtual int getType();
        virtual int getNumValues();
        virtual void receive(float* values);
};

#endif //HANDLER_H