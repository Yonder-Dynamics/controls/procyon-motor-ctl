# Motor Control Drivers
This subdirectory contains drivers for rover motor control.

The new control subsystem drivers are intended to be implemented as modules to which can be loaded into `main.py` and selected with a configuration file.

Motor control driver modules must export a `create` method to construct their helper objects. `create` takes a config dictionary and returns an initialized helper object.

## Inputs
The inputs directory contains modules for handling different input sources. Input source modules should provide a `create` function which returns an input handler object with a `begin` method. `begin` is a blocking call which calls the provided observer function with input whenever new input is received. Please see the `Redis` implementation for examples of this behavior.

Inputs currently supported are:
* Redis

## Protocols

The protocols directory contains modules for using different communication protocols. 

Protocols currently supported are:
* HashDelimited

## Devices
The devices directory contains modules for communicating with different control hardware. Device modules should provide a `create` function that returns a driver object implementing the driver methods. Please see the `SerialDriver` implementation for examples.

`Driver` methods:
* send(): send data to the device
* receive(): recieve data from the device
* connect(): initiate connection to the device
* close(): close the connection to the device
* fix(): attempt to fix connection issues
* reset(): clear any buffered input or output

Devices currently supported are:
* Serial
* File